# Client up project

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.2.3.

## Development Client

Run `npm i` for add node_modelus in project.

Run `ng build` for a build project.

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

# Server up 

This project was generated with [NODE]

## Development server

Run `npm i` for add node_modelus in project.

Run `npm run start` for a dev server. Navigate to `http://localhost:9000/`.
Run `npm run dev` for run server with nodemon. Navigate to `http://localhost:9000/`.