import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DASHBOARDComponent } from './dashboard/dashboard.component';
import { HOMEComponent } from './home/home.component';
import { ROOMComponent } from './room/room.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'HOME' },
  { path: 'HOME', component: HOMEComponent },
  { path: 'DASHBOARD', component: DASHBOARDComponent },
  { path: 'ROOM/:code', component: ROOMComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
