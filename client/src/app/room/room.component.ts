import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SocketwebService } from '../socketweb.service';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.scss']
})
export class ROOMComponent implements OnInit {

  valor = {
    username: '',
    message: ''
  }
  userActive: number;
  constructor(
    private activedRouter: ActivatedRoute,
    private webSocket: SocketwebService
  ) {
    let valor = this.activedRouter.snapshot.params.code;
    this.valor.username = localStorage.getItem('username');
    localStorage.setItem('roomCode', valor);
    this.listenMessage();
    this.listenUsers();
  }

  ngOnInit(): void {
  }

  send() {
    this.webSocket.emitEventMessage(this.valor);
  }

  listenMessage() {
    this.webSocket.outEvenMessage.subscribe((params) => {
      document.getElementById('actions').innerHTML = '';
      document.getElementById('output').innerHTML += `<p><strong>${params.username}</strong>: ${params.message}</p>`;
    });
  }

  listenUsers() {
    this.webSocket.outEvenUsers.subscribe((params) => {
      this.userActive = params;
    })
  }

}
