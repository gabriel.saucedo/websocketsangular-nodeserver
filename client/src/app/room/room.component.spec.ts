import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ROOMComponent } from './room.component';

describe('ROOMComponent', () => {
  let component: ROOMComponent;
  let fixture: ComponentFixture<ROOMComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ROOMComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ROOMComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
