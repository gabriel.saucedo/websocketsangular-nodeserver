import { Injectable, Output, EventEmitter } from '@angular/core';
import { Socket } from 'ngx-socket-io';

@Injectable({
  providedIn: 'root'
})
export class SocketwebService extends Socket {

  @Output() outEvenMessage: EventEmitter<any> = new EventEmitter();
  @Output() outEvenTyping: EventEmitter<any> = new EventEmitter();
  @Output() outEvenUsers: EventEmitter<any> = new EventEmitter();
  constructor() {
    super({
      url: 'http://localhost:9000',
      options: {
        query: {
          username: localStorage.getItem('username'),
          nameRoom: localStorage.getItem('roomCode')
        }
      }
    });
    this.listenMessage();
    this.listenTyping();
    this.listenUsers();
  }

  listenMessage = () => {
    this.ioSocket.on('chat:message', res => this.outEvenMessage.emit(res));
  }

  emitEventMessage = (payload = {}) => {
    this.ioSocket.emit('chat:message', payload)
  }
  listenTyping = () => {
    this.ioSocket.on('chat:typing', res => this.outEvenTyping.emit(res));
  }

  emitEventTyping = (payload = '') => {
    this.ioSocket.emit('chat:typing', payload)
  }
  listenUsers = () => {
    this.ioSocket.on('userconnect', res => this.outEvenUsers.emit(res));
  }
}
