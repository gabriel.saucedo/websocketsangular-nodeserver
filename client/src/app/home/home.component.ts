import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HOMEComponent implements OnInit {

  valor = {
    username: ''
  };
  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  send() {
    localStorage.setItem('username', this.valor.username);
    this.router.navigate(['DASHBOARD']);
  }

  keyupMessage() {

  }

}
