import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DASHBOARDComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  goToRoom1() {
    this.router.navigate(['ROOM/sala-1'])
  }

  goToRoom2() {
    this.router.navigate(['ROOM/sala-2'])
  }

  goToRoom3() {
    this.router.navigate(['ROOM/sala-3'])
  }

}
