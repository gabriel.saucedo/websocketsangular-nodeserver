const express = require('express');
const app = express();
const cors = require('cors');
app.use(cors());
const options = {
  cors: {
    origin: 'http://localhost:4200',
  },
};
const server = require('http').Server(app)
const io = require('socket.io')(server, options);

app.get('/', function (req, res) {
  res.send('Hello World!');
});

io.on('connection', (socket) => {
  const users = [];
  const user_1 = [];
  const user_2 = [];
  const user_3 = [];
  for (let [id, socket] of io.of('/').sockets) {
    users.push({
      userID: id,
      userName: socket.handshake.query.username !== '' ? socket.handshake.query.username : 'Guest User ' + id,
      userRoom: socket.handshake.query.nameRoom
    });
  }

  socket.join(socket.handshake.query.nameRoom);
  for (const key in users) {
    switch (users[key].userRoom) {
      case 'sala-1':
        user_1.push(users[key]);
      case 'sala-2':
        user_2.push(users[key]);
      case 'sala-3':
        user_3.push(users[key]);
      default:
        break;
    }
  }
  io.to(socket.handshake.query.nameRoom).emit('userconnect', users.length);


  socket.on('chat:message', (data) => {
    io.to(socket.handshake.query.nameRoom).emit('chat:message', data);
  })

  socket.on('chat:typing', (data) => {
    socket.broadcast.emit('chat:typing', data);
  })
  socket.on('disconnect', function () {
    for (const key in users) {
      if (users[key].userID == socket.id) {
        console.log(users[key].userName + ' disconnected');
        users.splice(key);
      }
    }
    
    for (const key in user_1) {
      if (user_1[key].userID == socket.id) {
        user_1.splice(key);
      }
    }
    for (const key in user_2) {
      if (user_2[key].userID == socket.id) {
        user_2.splice(key);
      }
    }
    for (const key in user_3) {
      if (user_3[key].userID == socket.id) {
        user_3.splice(key);
      }
    }
     
  })
});

server.listen(9000, () => {
  console.log('Server up in', 9000);
});